package org.test.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;
import jakarta.enterprise.context.ApplicationScoped;
import org.test.entity.UserEntity;

import java.util.List;

@ApplicationScoped
public class UserRepository implements PanacheRepository<UserEntity> {

    public List<UserEntity> findByName(String name){
        return list("name order by name desc", name);
    }

    public List<UserEntity> findLikeByName(String name){
        name = "%"+name+"%";
        return find("name like :name order by id desc",Parameters.with("name", name)).list();
    }

    public List<UserEntity> findLikeByEmail(String email){
        email = "%"+email+"%";
        return find("emailAddress like :email order by id desc",Parameters.with("email", email)).list();
    }
    

}
