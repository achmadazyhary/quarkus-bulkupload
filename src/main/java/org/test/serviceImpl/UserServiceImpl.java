package org.test.serviceImpl;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.logging.Log;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.test.dto.Users;
import org.test.entity.UserEntity;
import org.test.repository.UserRepository;
import org.test.response.ResponseData;
import org.test.response.ResponseMsg;
import org.test.service.UserService;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
@Transactional
public class UserServiceImpl implements UserService {

    @Inject
    private UserRepository userRepository;

    private ObjectMapper mapper = new ObjectMapper();

    public UserServiceImpl() {
    }

    @Override
    public ResponseData getAllUser() {
        ResponseData response = new ResponseData();
        response.setSuccess(true);
        response.setMessage("OK");
        List<UserEntity> users = userRepository.listAll();
        try {
            Log.info("users: " + mapper.writerWithDefaultPrettyPrinter().writeValueAsString(users));
        } catch (Exception e) {
        }
        response.setData(users);
        return response;
    }

    @Override
    public ResponseData getUserById(Long id) {
        ResponseData response = new ResponseData();
        response.setSuccess(true);
        response.setMessage("OK");
        Optional<UserEntity> user = userRepository.findByIdOptional(id);
        if (user.isEmpty()) {
            response.setSuccess(false);
            response.setMessage("User Id Not Found");
            return response;
        }
        response.setData(user.get());
        return response;
    }

    @Override
    public ResponseMsg createUser(Users users) {
        ResponseMsg response = new ResponseMsg();
        response.setSuccess(true);
        response.setMessage("OK");
        Date now = new Date();
        UserEntity user = new UserEntity();
        user.setName(users.getName());
        user.setEmail(users.getEmail());
        user.setCdate(now);
        user.setCby("system");
        user.setMdate(now);
        user.setMby("system");
        user.setEmailAddress(users.getEmail());
        userRepository.persist(user);
        return response;
    }

    @Override
    public ResponseMsg updateUser(Users users) {
        ResponseMsg response = new ResponseMsg();
        response.setSuccess(true);
        response.setMessage("OK");
        Optional<UserEntity> user = userRepository.findByIdOptional(users.getId());
        if (user.isEmpty()) {
            response.setSuccess(false);
            response.setMessage("User Id Not Found");
            return response;
        }
        Date now = new Date();
        UserEntity u = user.get();
        u.setName(users.getName());
        u.setEmail(users.getEmail());
        u.setMdate(now);
        u.setMby(u.getId().toString());
        userRepository.persist(u);
        return response;
    }

    @Override
    public ResponseMsg deleteUser(Long id) {
        ResponseMsg response = new ResponseMsg();
        response.setSuccess(true);
        response.setMessage("OK");
        Optional<UserEntity> user = userRepository.findByIdOptional(id);
        if (user.isEmpty()) {
            response.setSuccess(false);
            response.setMessage("User Id Not Found");
            return response;
        }
        userRepository.deleteById(id);
        return response;
    }

    @Override
    public ResponseData findByName(String name) {
        ResponseData response = new ResponseData();
        response.setSuccess(true);
        response.setMessage("OK");

        //cara 1
//        Map<String, Object> params = new HashMap<>();
//        params.put("name", name);
//        params.put("email", name+"@gmail.com");
//        List<UserEntity> u = userRepository.find("name = :name and email = :email", params).list();

        //cara 2
//        name = "%"+name+"%";
//        List<UserEntity> u2 = userRepository.find("name like :name and email like :email",
//                Parameters.with("name", name).and("email", name)).list();

        //cara3
//        List<UserEntity> users = userRepository.list("name", name);

        //cara4
        List<UserEntity> us = userRepository.findLikeByEmail(name);
        try {
            Log.info("us: " + mapper.writerWithDefaultPrettyPrinter().writeValueAsString(us));
        } catch (Exception e) {
        }

        response.setData(us);
        return response;
    }

    @Override
    public ResponseMsg processUploadFile(List<InputPart> inputParts) {
        ResponseMsg response = new ResponseMsg();
        response.setSuccess(true);
        response.setMessage("OK");
        try {
            for (InputPart inputPart : inputParts) {
                InputStream inputStream = inputPart.getBody(InputStream.class, null);

                List<UserEntity> users = new ArrayList<>();
                try (CSVParser csvParser = new CSVParser(new InputStreamReader(inputStream), CSVFormat.DEFAULT
                        .withFirstRecordAsHeader()
                        .withIgnoreHeaderCase()
                        .withTrim())) {
                    for (CSVRecord csvRecord : csvParser) {
                        UserEntity user = new UserEntity();
                        user.setName(csvRecord.get("name"));
                        user.setEmail(csvRecord.get("email"));
                        user.setCdate(new Date());
                        user.setCby("system");
                        user.setMdate(new Date());
                        user.setMby("system");
                        user.setEmailAddress(csvRecord.get("email"));
                        users.add(user);
                    }

                    userRepository.persist(users);
                    Log.info("Success Save User");
                }
            }

        } catch (Exception e) {
            Log.error("Failed to Read File, " + e);
            response.setSuccess(false);
            response.setMessage("Failed to Read File");
            return response;
        }

        return response;
    }
}
