package org.test.serviceImpl;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.logging.Log;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import org.test.dto.Products;
import org.test.entity.ProductEntity;
import org.test.entity.Status;
import org.test.entity.UserEntity;
import org.test.kafka.KafkaProducer;
import org.test.repository.ProductRepository;
import org.test.repository.UserRepository;
import org.test.response.ResponseData;
import org.test.response.ResponseMsg;
import org.test.service.ProductService;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
@Transactional
public class ProductServiceImpl implements ProductService {

    @Inject
    private ProductRepository productRepository;

    @Inject
    private UserRepository userRepository;

    private final ObjectMapper mapper = new ObjectMapper();

    @Inject
    KafkaProducer kafkaProducer;

    @Override
    public ResponseData getAllProducts() {
        ResponseData response = new ResponseData();
        response.setMessage("OK");
        response.setSuccess(true);
        List<ProductEntity> product = productRepository.listAll();
        response.setData(product);
        kafkaProducer.sendActivityToKafka("Get All Products");
        return response;
    }

    @Override
    public ResponseData getProductById(Long id) {
        ResponseData response = new ResponseData();
        response.setMessage("OK");
        response.setSuccess(true);
        ProductEntity product = productRepository.findById(id);
        if (product == null) {
            response.setSuccess(false);
            response.setMessage("Product Id Not Found");
            return response;
        }
        response.setData(product);
        kafkaProducer.sendActivityToKafka("Get Product By Id:"+ id.toString());
        return response;
    }

    @Override
    public ResponseMsg createProduct(Long userId, Products products) {
        ResponseMsg response = new ResponseMsg();
        response.setMessage("OK");
        response.setSuccess(true);

        Log.info("Check user");
        Optional<UserEntity> user = userRepository.findByIdOptional(userId);
        if (user.isEmpty()) {
            response.setMessage("UserId not found");
            response.setSuccess(false);
            return response;
        }

        Date now = new Date();
        ProductEntity p = new ProductEntity();
        p.setProductName(products.getProductName());
        p.setProductDesc(products.getProductDesc());
        p.setUserEntity(user.get());
        if (products.getStatus()) {
            p.setStatus(Status.ACTIVE);
        } else p.setStatus(Status.INACTIVE);
        p.setCby(user.get().getName());
        p.setCdate(now);
        p.setMby(user.get().getName());
        p.setMdate(now);
        try {
            Log.info("products: \n" + mapper.writerWithDefaultPrettyPrinter().writeValueAsString(p));
            productRepository.persist(p);
        } catch (Exception e) {
            Log.error("failed, " + e);
        }
        kafkaProducer.sendActivityToKafka("Save Product by "+user.get().getName());
        return response;
    }

    @Override
    public ResponseMsg updateProduct(Long userId, Products products) {
        ResponseMsg response = new ResponseMsg();
        response.setMessage("OK");
        response.setSuccess(true);

        Optional<UserEntity> user = userRepository.findByIdOptional(userId);
        if (user.isEmpty()) {
            response.setMessage("UserId not found");
            response.setSuccess(false);
            return response;
        }

        Optional<ProductEntity> checkProduct = productRepository.findByIdOptional(products.getId());
        if (checkProduct.isPresent()) {
            response.setMessage("Product not found");
            response.setSuccess(false);
            return response;
        }

        Date now = new Date();
        ProductEntity p = checkProduct.get();
        p.setProductName(products.getProductName());
        p.setProductDesc(products.getProductDesc());
        p.setMby(user.get().getName());
        p.setMdate(now);
        productRepository.persist(p);
        kafkaProducer.sendActivityToKafka("Update Product "+checkProduct.get().getId() + "By "+user.get().getName());
        return response;
    }

    @Override
    public ResponseMsg deleteProduct(Long userId, Long id) {
        ResponseMsg response = new ResponseMsg();
        response.setMessage("OK");
        response.setSuccess(true);

        Optional<UserEntity> user = userRepository.findByIdOptional(userId);
        if (user.isEmpty()) {
            response.setMessage("UserId not found");
            response.setSuccess(false);
            return response;
        }

        Optional<ProductEntity> checkProduct = productRepository.findByIdOptional(id);
        if (checkProduct.isPresent()) {
            response.setMessage("Product not found");
            response.setSuccess(false);
            return response;
        }
        productRepository.deleteById(id);
        kafkaProducer.sendActivityToKafka("Delete Product "+checkProduct.get().getId() + "By "+user.get().getName());
        return response;
    }
}
