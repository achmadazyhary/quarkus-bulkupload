package org.test.kafka;

import io.quarkus.logging.Log;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;

@ApplicationScoped
public class KafkaProducer {

    @Inject
    @Channel("data-output")
    Emitter<String> emitter;

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public void sendActivityToKafka(String message) {
        Log.info("Received in Producer");
        emitter.send(message);
    }

}
