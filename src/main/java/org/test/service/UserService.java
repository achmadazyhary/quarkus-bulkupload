package org.test.service;

import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.test.dto.Users;
import org.test.response.ResponseData;
import org.test.response.ResponseMsg;

import java.io.InputStream;
import java.util.List;

public interface UserService {

    ResponseData getAllUser();
    ResponseData getUserById(Long id);
    ResponseMsg createUser(Users users);
    ResponseMsg updateUser(Users users);
    ResponseMsg deleteUser(Long id);
    ResponseData findByName(String name);
    ResponseMsg processUploadFile(List<InputPart> inputStream);

}
