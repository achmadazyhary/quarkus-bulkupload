package org.test.service;

import org.test.dto.Products;
import org.test.response.ResponseData;
import org.test.response.ResponseMsg;

public interface ProductService {

    ResponseData getAllProducts();
    ResponseData getProductById(Long id);
    ResponseMsg createProduct(Long userId, Products products);
    ResponseMsg updateProduct(Long userId, Products products);
    ResponseMsg deleteProduct(Long userId, Long id);
}
