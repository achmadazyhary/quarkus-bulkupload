package org.test.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
//import org.jboss.resteasy.reactive.server.multipart.MultipartFormDataInput;
import org.test.dto.Users;
import org.test.response.ResponseData;
import org.test.response.ResponseMsg;
import org.test.service.UserService;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("/users")
public class UsersResource {

    private final ObjectMapper mapper = new ObjectMapper();
    private HashMap<Long, Users> users = new HashMap<>();
    @Inject
    private UserService userService;

    @GET
    @Path("all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllUsers() {
        ResponseData response = userService.getAllUser();
//        response.setSuccess(true);
//        response.setMessage("Successfully get all users");
//        response.setData(users.values());
        return Response.ok(response).build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserById(@PathParam("id") Long id) {
//        ResponseData response = new ResponseData();
//        response.setMessage("User Not Found");
//        response.setSuccess(false);
//
//        Users user = users.get(id);
//        try {
//            Log.info("user: "+mapper.writerWithDefaultPrettyPrinter().writeValueAsString(user));
//        }catch (Exception e){
//        }
//        if(Objects.isNull(user)){
//            return Response.ok(response).build();
//        }
//        response.setMessage("Successfully get user");
//        response.setSuccess(true);
//        response.setData(user);

        ResponseData response = userService.getUserById(id);
        return Response.ok(response).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createUser(Users user) {
//        ResponseMsg response = new ResponseMsg();
//        response.setMessage("Successfully create new user");
//        response.setSuccess(true);
//        users.put(user.getId(), user);
        ResponseMsg response = userService.createUser(user);
        return Response.ok(response).build();
//        return Response.created(URI.create("/users/"+user.getId())).build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateUser(Users user) {
        ResponseMsg response = userService.updateUser(user);
        return Response.ok(response).build();
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response deleteUser(Long id) {
        ResponseMsg response = userService.deleteUser(id);
        return Response.ok(response).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("name/{name}")
    public Response findUserByName(String name) {
        ResponseData response = userService.findByName(name);
        return Response.ok(response).build();
    }

    @POST
    @Path("upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFile(@MultipartForm MultipartFormDataInput input) {
        try {
            Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
            List<InputPart> inputParts = uploadForm.get("file");
            userService.processUploadFile(inputParts);
            return Response.ok("File uploaded successfully").build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error uploading file: " + e.getMessage())
                    .build();
        }
    }

}
