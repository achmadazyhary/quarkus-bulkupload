package org.test.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.test.dto.Products;
import org.test.dto.Users;
import org.test.response.ResponseData;
import org.test.response.ResponseMsg;
import org.test.service.ProductService;

import java.util.HashMap;

@Path("/product")
public class ProductResouce {
    private final ObjectMapper mapper = new ObjectMapper();
    private HashMap<Long, Users> users = new HashMap<>();

    @Inject
    private ProductService productService;

    @GET
    @Path("all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllProducts() {
        ResponseData response = productService.getAllProducts();
        return Response.ok(response).build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProductById(@PathParam("id") Long id) {
        ResponseData response = productService.getProductById(id);
        return Response.ok(response).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response createProduct(@PathParam("id") Long userId, Products product){
        ResponseMsg response = productService.createProduct(userId, product);
        return Response.ok(response).build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response updateProduct(@PathParam("id") Long userId, Products products){
        ResponseMsg response = productService.updateProduct(userId, products);
        return Response.ok(response).build();
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{userid}/{id}")
    public Response deleteProduct(@PathParam("userid") Long userId, @PathParam("id") Long id){
        ResponseMsg response = productService.deleteProduct(userId,id);
        return Response.ok(response).build();
    }

}
